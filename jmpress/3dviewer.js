var org = {};
if (org){
	org.v3dmodels = [];
	org.v3d = function(container, options){
		var self = this;
		self.container = $(container);
		self.data = self.container.data();
		self.options = options || {};
		self.options.isRTIE = navigator.msMaxTouchPoints;
		//self.options.isRTIE = true;
		self.state = {
			allowAnimate: false,
			inited: false
		};

		self.imageToCanvas = function( image ) {
			var width = image.width;
			var height = image.height;

			var canvas = document.createElement( 'canvas' );

			canvas.width = width;
			canvas.height = height;
			var context = canvas.getContext( '2d' );
			context.drawImage( image, 0, 0, width, height );

			return canvas;
		};

		self.canvasToImage = function(){
			var canvas = self.container.children('canvas');
			if (canvas.length === 1){
				canvas = canvas[0];
				return canvas.toDataURL();
			}
		};

		self.base64ToUint8Array = function(base64string){
			var BASE64_MARKER = ';base64,';
			var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
			var base64 = dataURI.substring(base64Index);
			var raw = window.atob(base64);
			var rawLength = raw.length;
			var array = new Uint8Array(new ArrayBuffer(rawLength));

			for(i = 0; i < rawLength; i++) {
				array[i] = raw.charCodeAt(i);
			}
			return array;
		};

		self.colorify = function( ctx, width, height, color, a ) {

			var r = color.r;
			var g = color.g;
			var b = color.b;

			var imageData = ctx.getImageData( 0, 0, width, height );
			var data = imageData.data;
			for ( var y = 0; y < height; y ++ ) {
				for ( var x = 0; x < width; x ++ ) {
					var index = ( y * width + x ) * 4;

					data[ index ]     *= r;
					data[ index + 1 ] *= g;
					data[ index + 2 ] *= b;
					data[ index + 3 ] *= a;

				}
			}
			ctx.putImageData( imageData, 0, 0 );
		};

		self.addSpriteOnLoad = function(img, color, position){
			var canvas = self.imageToCanvas( img );
			var context = canvas.getContext( '2d' );
			self.colorify( context, canvas.width, canvas.height, color, 1 );
			var dataUrl = canvas.toDataURL();
			var map = THREE.ImageUtils.loadTexture( dataUrl );
			var material = new THREE.SpriteMaterial({ map: map, useScreenCoordinates: false });
			var sphere = new THREE.Sprite(material);
			sphere.position = position;
			sphere.scale.set( 0.5, 0.5, 1.0 ); 
			self.scene.add( sphere );
		};
	};

	org.v3d.prototype.makePreview = function(callback){
		var self = this;
		self.init(function(){
			self.render();
			self.controls.update();
			var base64img = self.canvasToImage();
			callback(null, base64img);
		});
	};	

	org.v3d.prototype.start = function(callback){
		var self = this;
		if (!self.state.allowAnimate){
			self.state.allowAnimate = true;
			//if (!self.state.inited){
				self.init(callback);
			//}
			self.animate(true);
		}
	};
	org.v3d.prototype.stop = function(){
		var self = this;
		self.state.allowAnimate = false;
		if (self.data.thumb){
			var th = document.createElement("img");
			th.src = self.data.thumb;
			th.style.margin = "0 auto";
			th.className = "img img-responsive";
			//self.scene.remove(self.cam);self.cam = null;
			//self.scene.remove(self.contorls); self.controls = null;
			//self.scene.remove(self.light); self.light = null;
			
			_.each(self.scene.children, function( object ) {
				self.scene.remove(object);
				if (typeof object.dispose === 'function'){ object.dispose();}
				if (object.geometry) { object.geometry.dispose();}
				if (object.texture) { object.texture.dispose();}
				if (object.material) { object.material.dispose();}
				//self.renderer.deallocateObject(object);

		    });
			//self.scene = null;
			self.container.children('canvas').remove();
			self.container.html(th);
			//self.scene = null;
			self = null;
		}
	};

	org.v3d.prototype.init = function(callback){
		var self = this;

		self.scene = new THREE.Scene();
		
		/*self.stats = new Stats();
		self.container.append( stats.domElement );*/

		self.renderer = Detector.webgl ? 
			new THREE.WebGLRenderer({ alpha: true, antialias: true, preserveDrawingBuffer: true }) :
			new THREE.CanvasRenderer({ alpha: true, antialias: true });
		self.renderer.setSize( self.container.width(), self.container.height() );
		self.renderer.setClearColor( 0x000000, 0 );

		self.cam = new THREE.PerspectiveCamera( 45, self.container.width() / self.container.height(), 1, 1000 );
		self.cam.position.set( 0, 0, 3 );

		//if (!self.options.isRTIE){
		self.controls = new THREE.OrbitControls( self.cam );
		self.controls.addEventListener( 'change', self.render.bind(self) );

		self.light = new THREE.PointLight(0xffffff);
		self.scene.add(self.light);
		
		if (self.data.model){
			var loader;
			if (self.data.model.lastIndexOf('.stl') === self.data.model.length - 4){
				loader = new THREE.STLLoader();
				loader.addEventListener( 'load', function ( event ) {
					var geometry = event.content;
					var material = new THREE.MeshPhongMaterial( { ambient: 0x555555, color: 0xAAAAAA, specular: 0x111111, shininess: 200 } );
					var mesh = new THREE.Mesh( geometry, material );
					geometry.computeBoundingBox();
					/*mesh.position.set( 0, 0, 0 );
					var max = {
						x: geometry.boundingBox.max.x,
						y: geometry.boundingBox.max.y,
						z: geometry.boundingBox.max.z
					}
					max.max = max.x;
					if (max.y > max.max) { max.max = max.y }
					if (max.z > max.max) { max.max = max.z }					
					self.cam.position.set( max.x > 0.5 ? 2*max.x : 1 , 0.5*max.y, max.z > 0.5 ? 2*max.max: 1 );
					self.scene.add( mesh );
					*/
					var bb = geometry.boundingBox;
					var scale = 1/(bb.max.x - bb.min.x);
					mesh.scale.set(scale,scale,scale);
					mesh.position.setX(-(bb.max.x + bb.min.x) * scale / 2 );
					mesh.position.setY(-(bb.max.y + bb.min.y) * scale / 2 );
					mesh.position.setZ(-(bb.max.z + bb.min.z) * scale / 2 );
					self.scene.add( mesh );
					if (typeof callback === "function") callback(null, self);
				});
				loader.load(self.data.model);
			}
			else if (self.data.model.lastIndexOf('.obj') === self.data.model.length - 4){
				loader = new THREE.OBJMTLLoader();
				var obj = self.data.model.slice(0, self.data.model.length - 4);
				var mtl = obj + '/texture.mtl';
				obj += "/model.obj";
				loader.load( obj, mtl, function ( object ) {
					if (object){
						var max = {x:0,y:0,z:0}, min = {x:0,y:0,z:0};
						object.traverse(function ( child ) {
					        if ( child instanceof THREE.Mesh ) {
					        	var cg = child.geometry;
					            cg.computeBoundingBox();
					            if (cg.boundingBox.max.x > max.x){ max.x = cg.boundingBox.max.x; }
				            	if (cg.boundingBox.max.y > max.y){ max.y = cg.boundingBox.max.y; }
				            	if (cg.boundingBox.max.z > max.z){ max.z = cg.boundingBox.max.z; }
				            	if (cg.boundingBox.min.x < min.x){ min.x = cg.boundingBox.min.x; }
				            	if (cg.boundingBox.min.y < min.y){ min.y = cg.boundingBox.min.y; }
				            	if (cg.boundingBox.min.z < min.z){ min.z = cg.boundingBox.min.z; }
					        }	
					        
					    });
					    var scale = 1/(max.x - min.x);
					    object.scale.set(scale, scale, scale);
					    object.position.setX(-(max.x + min.x) * scale / 2 );
					    object.position.setY(-(max.y + min.y) * scale / 2 );
					    object.position.setZ(-(max.z + min.z) * scale / 2 );
						self.scene.add( object );
					}
				});
			}
			else if (self.data.model.lastIndexOf('.pdb') === self.data.model.length - 4){
				loader = new THREE.PDBLoader();
				loader.load( self.data.model , function ( geometry, geometryBonds ) {
					var offset = THREE.GeometryUtils.center( geometry );
					geometryBonds.applyMatrix( new THREE.Matrix4().makeTranslation( offset.x, offset.y, offset.z ) );
					var max = {x:0,y:0,z:0}, min = {x:0,y:0,z:0};
					for ( var i = 0; i < geometry.vertices.length; i ++ ) {
						var position = geometry.vertices[ i ];
						if (position.x > max.x){max.x = position.x;}
						if (position.y > max.y){max.y = position.y;}
						if (position.z > max.z){max.z = position.z;}
						if (position.x < min.x){min.x = position.x;}
						if (position.y < min.y){min.y = position.y;}
						if (position.z < min.z){min.z = position.z;}
						
						var color = geometry.colors[ i ];
						var element = geometry.elements[ i ];
						if (Detector.webgl){
							var sphereGeometry = Detector.webgl ? new THREE.SphereGeometry(0.3, 16, 8 ) : new THREE.SphereGeometry(0.3, 8, 8 );
							var material = new THREE.MeshLambertMaterial( { shading: THREE.SmoothShading } );
							material.color = color;

							var sphere = new THREE.Mesh( sphereGeometry, material );
							sphere.position = position;
							self.scene.add( sphere );
						} else {
							var baseSprite = new Image();
							baseSprite.onload = (function(col,pos){
								return function() {
									self.addSpriteOnLoad(baseSprite, col, pos);
								};
							})(color,position);
							baseSprite.src = "3js/img/sprites/ball.png";
						}
					}
					for ( var i = 0; i < geometryBonds.vertices.length; i += 2 ) {

						var start = geometryBonds.vertices[ i ];
						var end = geometryBonds.vertices[ i + 1 ];
						if (Detector.webgl){
							var material = new THREE.MeshLambertMaterial( { color: 0x0000ff, shading: THREE.SmoothShading } );
							var direction = new THREE.Vector3().subVectors( end, start );
						    var arrow = new THREE.ArrowHelper( direction.clone().normalize(), start );
						    var edgeGeometry = new THREE.CylinderGeometry( 0.1, 0.1, direction.length(), 6, 0 );
						    var edge = new THREE.Mesh(edgeGeometry, material);
						    edge.rotation = arrow.rotation.clone();
						    edge.position = new THREE.Vector3().addVectors( start, direction.multiplyScalar(0.5) );

						    self.scene.add(edge);
						} else {
							var material = new THREE.LineBasicMaterial({color: 0x0000ff});
							var geometry = new THREE.Geometry();
						    geometry.vertices.push(start);
						    geometry.vertices.push(end);
						    var line = new THREE.Line(geometry, material);
						    self.scene.add(line);
						}
					}
					self.controls.center.setX((max.x + min.x) / 2);
					self.controls.center.setY((max.y + min.y) / 2);
					self.controls.center.setZ((max.z + min.z) / 2);

					self.cam.position.set( max.x - min.x, max.y - min.y, max.z - min.z );
					if (typeof callback === "function") callback(null,self);
				});
			}
			else {
				loader = null;
				if (typeof callback === "function") callback('invalid file');
			}
		}

		self.container.html( self.renderer.domElement );
		window.addEventListener( 'resize', self.onWindowResize.bind(self), false );
		//self.renderer.render( self.scene, self.cam );
		self.state.inited = true;
	};
	org.v3d.prototype.animate = function(firstTime) {
		var self = this;
		if (self.state.allowAnimate){
			requestAnimationFrame (self.animate.bind(self));
			if (firstTime){
				self.render();	
			}
			if (self.controls){
				self.controls.update();
			}
		}
	};

	org.v3d.prototype.render = function() {
		var self = this;
		/*if (self.options.isRTIE){
			var timer = Date.now() * 0.0005;
			var target = new THREE.Vector3(0,0,0);
			self.cam.position.x = 2 * Math.cos( timer );         
			self.cam.position.z = 2 * Math.sin( timer );
			self.cam.lookAt( target );
		}*/
		if (self.light) {self.light.position = self.cam.position};
		self.renderer.render( self.scene, self.cam );
	};

	org.v3d.prototype.onWindowResize = function(){
		var self = this;
		self.cam.aspect = self.container.width() / self.container.height();
		self.cam.updateProjectionMatrix();

		self.renderer.setSize( self.container.width(), self.container.height() );
	};
}