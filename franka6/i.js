var i = i || {};
i.conf = i.conf || {};
i.conf.cnt = "#content";
i.conf.ttc = ".tt-content";
i.conf.room = ".room";
i.conf.floors = ".floors";
i.conf.num = ".num";

i.getEmptyUserImage = function () {
	var images = ["img/user-icon-1.png", "img/user-icon-2.png", "img/user-icon-3.png"];
	var html = '<img src="' + images[Math.ceil(Math.random() * images.length)] + '" alt="user" />';
	return html;
};
i.setCookie = function (c_name, value, exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
};
i.getCookie = function (c_name) {
	var i, x, y, ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == c_name) {
			return unescape(y);
		}
	}
};
i.toggleTooltip = function(e) {
	var tt = $(e).children(i.conf.ttc);
	if (tt.is(':hidden')){
		$(i.conf.ttc).fadeOut("slow");
		tt.fadeIn("slow", function(){
			
		});
	} else {
		tt.fadeOut("slow", function(){
			
		});
	}
};
i.goToFloor = function (e){
	var floors = $(i.conf.floors);
	var curFloor = $(i.conf.num + '.active');
	var curFloorN = parseInt(curFloor.html());
	var targetFloorN = parseInt($(e).html());
	var diff = Math.abs(curFloorN - targetFloorN);
	if (diff > 0)
	{
		$(i.conf.ttc).fadeOut("slow");
		var duration = diff * 1000;
		var height = 881*(floors.children().size() - targetFloorN);
		$(i.conf.floors).animate({scrollTop : height},'slow',function(){
			$(i.conf.num).removeClass('active');
			$(e).addClass('active');
			i.setCookie("floor-num", targetFloorN, 365);
		});
	}
}
$(document).ready(function(){
	var floors = $(i.conf.floors);
	var florNum = i.getCookie("floor-num");
	if (typeof florNum != "undefined")
	{
		$(i.conf.num).removeClass('active');
		$(i.conf.num + '-' + florNum).addClass('active');
		floors.scrollTop(881*(floors.children().size() - parseInt(florNum)));
	}
	else
	{
		floors.scrollTop(881*(floors.children().size()));
	}
	
	$(i.conf.room).on('click', function(){
		i.toggleTooltip(this);
	});
	$(i.conf.num).on('click', function(){
		i.goToFloor(this);
	});
});